package pl.sdacademy.recipe;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import pl.sdacademy.recipe.controller.ProductController;
import pl.sdacademy.recipe.mapper.ProductDTOMapper;
import pl.sdacademy.recipe.model.Product;
import pl.sdacademy.recipe.repository.ProductRepository;
import pl.sdacademy.recipe.service.ProductService;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(ProductController.class)
public class ProductControllerTests {


    @TestConfiguration
    static class ProductServiceImplTestContextConfiguration {
        @Bean
        public ProductService productService(ProductRepository productRepository,
                                             ProductDTOMapper productDTOMapper) {
            return new ProductService(productRepository, productDTOMapper);
        }

        @Bean
        public ProductDTOMapper productDTOMapper() {
            return new ProductDTOMapper();
        }
    }

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ProductRepository productRepository;

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductDTOMapper productDTOMapper;


    @Test
    public void create_product_then_find_it_by_id() throws Exception {
        //given
        Product product = new Product(null, "marchew", "warzyywo", 1.99);

        //when
        when(productRepository.save(any(Product.class))).thenReturn(product);
        when(productRepository.findById(any(Integer.class))).thenReturn(Optional.of(product));

        //then
        mvc.perform(post("/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"marchew\"," +
                        "\"type\":\"warzywo\"," +
                        "\"price\": 1.99}"))
                .andExpect(status().isOk());
    }


    @Test
    public void create_products_then_return_all() throws Exception {

        //given
        Product product1 = new Product(null, "marchew", "warzywo", 1.99);
        Product product2 = new Product(null, "pietruszka", "warzywo", 2.50);
        Product product3 = new Product(null, "pomidor", "warzywo", 3.00);

        List<Product> products = Arrays.asList(product1, product2, product3);

        //when
        when(productRepository.findAll()).thenReturn(products);

        //then
        mvc.perform(get("/products")).andExpect(status().isOk());
        mvc.perform(get("/products")).andExpect(jsonPath("$", hasSize(3)));
    }

    @Test
    public void create_recipe_then_delete_and_return_it() throws Exception {

        //given
        Product product = new Product(null, "marchew", "warzywo", 1.99);

        //when
        when(productRepository.findById(1)).thenReturn(Optional.of(product));

        //then
        mvc.perform(delete("/products/1"))
                .andExpect(status().isOk());
    }

    @Test
    public void delete_products_that_doesnt_exist_then_throw_NotFoundStatus() throws Exception {

        //given
        //when
        when(productRepository.findById(1)).thenReturn(Optional.empty());

        //then
        mvc.perform(delete("/products/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void update_product_by_changing_price_then_find_product_by_id() throws Exception {

        //given
        Product product = new Product(null, "marchew", "warzywo", 1.99);

        //when
        when(productRepository.findById(1)).thenReturn(Optional.of(product));

        //then
        mvc.perform(put("/products/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"marchew\"," +
                        "\"type\":\"warzywo\"," +
                        "\"price\": 2.10}"))
                .andExpect(status().isOk());

    }
}
