package pl.sdacademy.recipe;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import pl.sdacademy.recipe.dto.ProductDTO;
import pl.sdacademy.recipe.mapper.ProductDTOMapper;
import pl.sdacademy.recipe.model.Product;
import pl.sdacademy.recipe.repository.ProductRepository;
import pl.sdacademy.recipe.service.ProductService;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class ProductServiceTests {

    @TestConfiguration
    static class ProductServiceImplTestContexConfiguration {
        @Bean
        public ProductService productService(ProductRepository productRepository,
                                             ProductDTOMapper productDTOMapper) {
            return new ProductService(productRepository, productDTOMapper);
        }

        @Bean
        public ProductDTOMapper productDTOMapper() {
            return new ProductDTOMapper();
        }
    }

    @MockBean
    ProductRepository productRepository;

    @Autowired
    ProductDTOMapper productDTOMapper;

    @Autowired
    ProductService productService;


    @Test
    public void create_product_then_save() {
        //given
        Product product = new Product(null, "marchew", "warzyywo", 1.99);

        //when
        productService.addProduct(productDTOMapper.toDTO(product));

        //then
        Mockito.verify(productRepository).save(any(Product.class));
    }

    @Test
    public void create_products_then_return_them() {
        //given
        Product product1 = new Product(null, "marchew", "warzyywo", 1.99);
        Product product2 = new Product(null, "pomidor", "warzyywo", 3.00);

        //when
        when(productRepository.findAll()).thenReturn(Arrays.asList(product1, product2));
        List<ProductDTO> productsDTO = productService.getProducts();

        //then
        assertEquals(2, productsDTO.size());
    }

    @Test
    public void create_product_then_update() {
        //given
        Product product = new Product(null, "marchew", "warzyywo", 1.99);
        ProductDTO productDTO = new ProductDTO(product.getId(), "marchew", "warzywo", 2.20);

        //when
        when(productRepository.findById(product.getId())).thenReturn(Optional.of(product));
        ProductDTO productDTOUpdated = productService.updateProduct(product.getId(), productDTO);

        //then
        assertEquals((Object)(2.20), productDTOUpdated.getPrice());


    }

}
