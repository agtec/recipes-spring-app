package pl.sdacademy.recipe;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import pl.sdacademy.recipe.dto.RecipeDTO;
import pl.sdacademy.recipe.mapper.RecipeDTOMapper;
import pl.sdacademy.recipe.model.Product;
import pl.sdacademy.recipe.model.Recipe;
import pl.sdacademy.recipe.repository.ProductRepository;
import pl.sdacademy.recipe.repository.RecipeRepository;
import pl.sdacademy.recipe.service.RecipeService;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class RecipeServiceTests {

    @TestConfiguration
    static class RecipeServiceImplTestContexConfiguration {
        @Bean
        public RecipeService recipeService(RecipeRepository recipeRepository,
                                           RecipeDTOMapper recipeDTOMapper,
                                           ProductRepository productRepository) {
            return new RecipeService(recipeRepository, recipeDTOMapper, productRepository);
        }

        @Bean
        public RecipeDTOMapper recipeDTOMapper() {
            return new RecipeDTOMapper();
        }
    }

    @MockBean
    RecipeRepository recipeRepository;

    @MockBean
    ProductRepository productRepository;

    @Autowired
    RecipeDTOMapper recipeDTOMapper;

    @Autowired
    RecipeService recipeService;

    @Test
    public void create_recipe_then_save() {

        //given
        Product product = new Product(null, "marchew", "warzyywo", 1.99);
        Recipe recipe = new Recipe(null, "zupa", "zupa marchewkowa", 2000F,
                Arrays.asList(product), 50, 4);

        //when
        when((productRepository.getByName("marchew"))).thenReturn(Optional.of(product));
        recipeService.addRecipe(recipeDTOMapper.toDTO(recipe));

        //then
        Mockito.verify(recipeRepository).save(any(Recipe.class));
    }

    @Test
    public void find_all_recipes() {
        // given
        Product product1 = new Product(null, "marchew", "warzyywo", 1.99);
        Product product2 = new Product(null, "pietruszka", "warzyywo", 2.50);
        Recipe recipe1 = new Recipe(null, "zupa", "zupa marchewkowa", 2000F,
                Arrays.asList(product1, product2), 50, 4);
        Recipe recipe2 = new Recipe(null, "zupa", "zupa pomidorowa", 2000F,
                Arrays.asList(product1), 55, 4);

        //when
        when((productRepository.getByName("marchew"))).thenReturn(Optional.of(product1));
        when((productRepository.getByName("pietruszka"))).thenReturn(Optional.of(product2));
        recipeService.addRecipe(recipeDTOMapper.toDTO(recipe1));
        recipeService.addRecipe(recipeDTOMapper.toDTO(recipe2));
        when(recipeRepository.findAll()).thenReturn(Arrays.asList(recipe1, recipe2));
        List<Recipe> recipes = recipeDTOMapper.toEntityList(recipeService.getAllRecipes());

        //then
        assertEquals(2, recipes.size());
    }

    @Test
    public void create_recipe_then_update() {
        //given
        Product product = new Product(null, "marchew", "warzyywo", 1.99);
        Recipe recipe = new Recipe(null, "zupa", "zupa marchewkowa", 2000F,
                Arrays.asList(product), 50, 4);
        RecipeDTO recipeDTO = new RecipeDTO(null, "zupa", "zupa marchewkowa", 2500F,
                Arrays.asList(product.getName()), 55, 4);

        //when
        when((productRepository.getByName("marchew"))).thenReturn(Optional.of(product));
        recipeService.addRecipe(recipeDTOMapper.toDTO(recipe));
        when(recipeRepository.findById(recipe.getId())).thenReturn(Optional.of(recipe));
        recipeService.updateRecipe(recipe.getId(), recipeDTO);

        //then
        assertEquals((Object) 55, recipeDTO.getPreparationTime());
    }

    @Test
    public void delete_recipe_then_return_empty_list() {
        //given
        Product product = new Product(null, "marchew", "warzyywo", 1.99);
        Recipe recipe = new Recipe(null, "zupa", "zupa marchewkowa", 2000F,
                Arrays.asList(product), 50, 4);

        //when
        when((productRepository.getByName("marchew"))).thenReturn(Optional.of(product));
        recipeService.addRecipe(recipeDTOMapper.toDTO(recipe));
        when(recipeRepository.findById(recipe.getId())).thenReturn(Optional.of(recipe));
        recipeService.deleteRecipe(recipe.getId());
        List<RecipeDTO> recipesDTO = recipeService.findByName("zupa");

        //then
        assertEquals(0, recipesDTO.size());
    }

    @Test
    public void create_recipe_then_find_by_parameter_and_value() {
        //given
        Product product = new Product(null, "marchew", "warzyywo", 1.99);
        Recipe recipe = new Recipe(null, "zupa", "zupa marchewkowa", 2000F,
                Arrays.asList(product), 50, 4);

        //when
        when((productRepository.getByName("marchew"))).thenReturn(Optional.of(product));
        recipeService.addRecipe(recipeDTOMapper.toDTO(recipe));
        when(recipeRepository.findById(recipe.getId())).thenReturn(Optional.of(recipe));
        when(recipeRepository.findByPreparationTime(any(Integer.class))).thenReturn(Arrays.asList(recipe));
        List<RecipeDTO> recipeDTOS = recipeService.findBy("preparationTime", "50");

        //then
        assertEquals(1, recipeDTOS.size());
    }

}
