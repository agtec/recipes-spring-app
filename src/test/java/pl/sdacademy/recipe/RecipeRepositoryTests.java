package pl.sdacademy.recipe;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import pl.sdacademy.recipe.model.Product;
import pl.sdacademy.recipe.model.Recipe;
import pl.sdacademy.recipe.repository.ProductRepository;
import pl.sdacademy.recipe.repository.RecipeRepository;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class RecipeRepositoryTests {

    @Autowired
    TestEntityManager entityManager;

    @Autowired
    private RecipeRepository recipeRepository;

    @Autowired
    private ProductRepository productRepository;

    @Test
    public void when_no_data_return_empty_List() {
        //given
        //when
        List<Recipe> recipes = recipeRepository.findAll();

        //then
        assertEquals(0, recipes.size());
    }

    @Test
    public void save_recipe_then_return_proper_one() {
        //given
        Product product = new Product(1, "marchew", "warzyywo", 1.99);
        Recipe recipe = new Recipe(1, "zupa", "zupa marchewkowa", 2000F, Arrays.asList(product), 50, 4);

        //when
        productRepository.save(product);
        recipeRepository.save(recipe);
        List<Recipe> addedRecipes = recipeRepository.findByName("zupa");

        //then
        assertEquals(1, addedRecipes.size());
    }

    @Test
    public void find_recipe_by_preparation_time_then_return_proper_one() {
        //given
        Product product = new Product(null, "marchew", "warzyywo", 1.99);
        Recipe recipe = new Recipe(null, "zupa", "zupa marchewkowa", 2000F, Arrays.asList(product), 50, 4);

        //when
        productRepository.save(product);
        recipeRepository.save(recipe);
        List<Recipe> foundRecipes = recipeRepository.findByPreparationTime(50);

        //then
        assertEquals(1, foundRecipes.size());

    }

    @Test
    public void find_recipe_by_product_name_then_return_proper_one() {
        //given
        Product product = new Product(null, "marchew", "warzyywo", 1.99);
        Recipe recipe = new Recipe(null, "zupa", "zupa marchewkowa", 2000F, Arrays.asList(product), 50, 4);

        //when
        productRepository.save(product);
        recipeRepository.save(recipe);
        List<Recipe> foundRecipes = recipeRepository.findByProducts_name("marchew");

        //then
        assertEquals(1, foundRecipes.size());

    }

    @Test
    public void find_recipe_by_product_name_then_return_proper_one_v2() {
        //given
        Product product = new Product(null, "marchew", "warzyywo", 1.99);
        Recipe recipe = new Recipe(null, "zupa", "zupa marchewkowa", 2000F, Arrays.asList(product), 50, 4);

        //when
        entityManager.persist(product);
        entityManager.persist(recipe);
        List<Recipe> foundRecipes = recipeRepository.findByProducts_name("marchew");

        //then
        assertEquals(1, foundRecipes.size());

    }

    @Test
    public void delete_recipe_then_return_empty_list() {
        //given
        Product product = new Product(null, "marchew", "warzyywo", 1.99);
        Recipe recipe = new Recipe(null, "zupa", "zupa marchewkowa", 2000F, Arrays.asList(product), 50, 4);

        //when
        productRepository.save(product);
        recipeRepository.save(recipe);
        recipeRepository.deleteById(recipe.getId());
        List<Recipe> recipes = recipeRepository.findAll();

        //then
        assertEquals(0, recipes.size());

    }
}
