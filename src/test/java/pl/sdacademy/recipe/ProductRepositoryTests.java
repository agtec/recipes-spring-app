package pl.sdacademy.recipe;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import pl.sdacademy.recipe.model.Product;
import pl.sdacademy.recipe.repository.ProductRepository;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class ProductRepositoryTests {

    @Autowired
    private ProductRepository productRepository;

    @Test
    public void when_no_data_return_empty_List() {
        //given
        //when
        List<Product> products = productRepository.findAll();

        //then
        assertEquals(0, products.size());
    }

    @Test
    public void save_product_then_return_proper_one() {
        //given
        Product product = new Product(1, "marchew", "warzyywo", 1.99);

        //when
        productRepository.save(product);
        List<Product> addedProducts = productRepository.findByName("marchew");

        //then
        assertEquals(1, addedProducts.size());
    }

    @Test
    public void find_product_by_name_then_return_proper_one() {
        //given
        Product product = new Product(null, "marchew", "warzyywo", 1.99);

        //when
        productRepository.save(product);
        List<Product> foundProducts = productRepository.findByName("marchew");

        //then
        assertEquals(1, foundProducts.size());
    }

    public void delete_product_then_return_empty_list() {
        //given
        Product product = new Product(null, "marchew", "warzyywo", 1.99);

        //when
        productRepository.saveAndFlush(product);
        productRepository.deleteById(product.getId());
        List<Product> products = productRepository.findAll();

        //then
        assertEquals(0, products.size());
    }
}
