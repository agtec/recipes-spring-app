package pl.sdacademy.recipe;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import pl.sdacademy.recipe.controller.RecipeController;
import pl.sdacademy.recipe.mapper.RecipeDTOMapper;
import pl.sdacademy.recipe.model.Product;
import pl.sdacademy.recipe.model.Recipe;
import pl.sdacademy.recipe.repository.ProductRepository;
import pl.sdacademy.recipe.repository.RecipeRepository;
import pl.sdacademy.recipe.service.RecipeService;

import java.util.*;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(RecipeController.class)
public class RecipeControllerTests {

    @TestConfiguration
    static class RecipeServiceImplTestContextConfiguration {
        @Bean
        public RecipeService recipeService(RecipeRepository recipeRepository,
                                           RecipeDTOMapper recipeDTOMapper,
                                           ProductRepository productRepository) {
            return new RecipeService(recipeRepository, recipeDTOMapper, productRepository);
        }

        @Bean
        public RecipeDTOMapper recipeDTOMapper() {
            return new RecipeDTOMapper();
        }
    }

    @Autowired
    private MockMvc mvc;

    @MockBean
    private RecipeRepository recipeRepository;

    @MockBean
    private ProductRepository productRepository;

    @Autowired
    private RecipeService recipeService;

    @Autowired
    private RecipeDTOMapper recipeDTOMapper;


    @Test
    public void create_recipe_then_find_it_by_id() throws Exception {
        //given
        Product product = new Product(null, "marchew", "warzyywo", 1.99);
        Recipe recipe = new Recipe(null, "zupa", "zupa marchewkowa", 2000F,
                Arrays.asList(product), 50, 4);

        //when
        when(recipeRepository.save(any(Recipe.class))).thenReturn(recipe);
        when(recipeRepository.findById(any(Integer.class))).thenReturn(Optional.of(recipe));

        //then
        mvc.perform(post("/recipes")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"zupa\"," +
                        "\"description\": \"zupa marchewkowa\"," +
                        "\"type\":\"warzywo\","
                        + "\"calories\": 2000," +
                        "\"productNames\": [\"marchew\"]," +
                        "\"preparationTime\": 50," +
                        "\"servings\": 4}"))
                .andExpect(status().isOk());

    }

    @Test
    public void create_recipes_then_return_all() throws Exception {

        //given
        Product product1 = new Product(null, "marchew", "warzywo", 1.99);
        Product product2 = new Product(null, "pietruszka", "warzywo", 2.50);
        Product product3 = new Product(null, "pomidor", "warzywo", 3.00);
        Recipe recipe1 = new Recipe(null, "zupa", "zupa marchewkowa", 2000F,
                Arrays.asList(product1, product2), 50, 4);
        Recipe recipe2 = new Recipe(null, "zupa", "zupa pomidorowa", 2500F,
                Arrays.asList(product1, product2, product3), 45, 4);

        List<Recipe> recipes = Arrays.asList(recipe1, recipe2);

        //when
        when(recipeRepository.findAll()).thenReturn(recipes);

        //then
        mvc.perform(get("/recipes")).andExpect(status().isOk());
        mvc.perform(get("/recipes")).andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    public void create_recipe_then_delete_and_return_it() throws Exception {

        //given
        Product product1 = new Product(null, "marchew", "warzywo", 1.99);
        Product product2 = new Product(null, "pietruszka", "warzywo", 2.50);
        Recipe recipe = new Recipe(null, "zupa", "zupa marchewkowa", 2000F,
                Arrays.asList(product1, product2), 50, 4);

        //when
        when(recipeRepository.findById(1)).thenReturn(Optional.of(recipe));

        //then
        mvc.perform(delete("/recipes/1"))
                .andExpect(status().isOk());
    }

    @Test
    public void delete_recipe_that_doesnt_exist_then_throw_NotFoundStatus() throws Exception {

        //given
        //when
        when(recipeRepository.findById(1)).thenReturn(Optional.empty());

        //then
        mvc.perform(delete("/recipes/1"))
                .andExpect(status().isNotFound());
    }


    @Test
    public void update_recipe_by_changing_calories_then_find_recipe_by_id() throws Exception {

        //given
        Product product1 = new Product(null, "marchew", "warzywo", 1.99);
        Product product2 = new Product(null, "pietruszka", "warzywo", 2.50);
        Recipe recipe = new Recipe(null, "zupa", "zupa marchewkowa", 2000F,
                Arrays.asList(product1, product2), 50, 4);

        //when
        when(productRepository.getByName("marchew")).thenReturn(Optional.of(product1));
        when(productRepository.getByName("pietruszka")).thenReturn(Optional.of(product2));
        when(recipeRepository.findById(1)).thenReturn(Optional.of(recipe));

        //then
        mvc.perform(put("/recipes/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"zupa\"," +
                        "\"description\":\"zupa marchewkowa\"," +
                        "\"calories\": 1800," +
                        "\"productNames\": [\"marchew\"]," +
                        "\"preparationTime\": 50," +
                        "\"servings\": 4}"))
                .andExpect(status().isOk());

    }

//    @GetMapping("/getByParameters")
//    public List<RecipeDTO> getRecipesByParameters(@RequestParam(required = false) Map<String, String> params) {
//        return recipeService.findBy(params);
//    }

    @Test
    public void create_recipes_then_get_selected_by_parameters() throws Exception {

        //given
        Product product1 = new Product(null, "marchew", "warzywo", 1.99);
        Product product2 = new Product(null, "pietruszka", "warzywo", 2.50);
        Product product3 = new Product(null, "pomidor", "warzywo", 3.00);
        Recipe recipe1 = new Recipe(null, "zupa", "zupa marchewkowa", 2000F,
                Arrays.asList(product1, product2), 50, 4);
        Recipe recipe2 = new Recipe(null, "zupa", "zupa pomidorowa", 2500F,
                Arrays.asList(product1, product2, product3), 45, 4);

        List<Recipe> recipes = Arrays.asList(recipe1);

        //when
        when(recipeRepository.findByDescription("zupa pomidorowa")).thenReturn(recipes);

        //then
        mvc.perform(get("/recipes/getByParameters?description=zupa pomidorowa")).andExpect(status().isOk());
        mvc.perform(get("/recipes/getByParameters?description=zupa pomidorowa")).andExpect(jsonPath("$", hasSize(1)));
    }
}
