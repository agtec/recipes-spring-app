package pl.sdacademy.recipe.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sdacademy.recipe.model.Product;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product, Integer> {

    List<Product> findByName(String name);
    Optional<Product> getByName(String name);

}
