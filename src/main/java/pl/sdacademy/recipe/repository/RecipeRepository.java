package pl.sdacademy.recipe.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sdacademy.recipe.model.Recipe;

import java.util.List;

public interface RecipeRepository extends JpaRepository<Recipe, Integer> {

    List<Recipe> findByName(String name);
    List<Recipe> findByPreparationTime(Integer preparationTime);
    List<Recipe> findByProducts_name(String name);
    List<Recipe> findByDescription(String description);
    List<Recipe> findByCalories(Float calories);
    List<Recipe> findByServings(Integer servings);
}
