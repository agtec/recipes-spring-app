package pl.sdacademy.recipe.dto;

import java.util.List;

public class RecipeDTO {

    private Integer id;
    private String name;
    private String description;
    private Float calories;
    private List<String> productNames;
    private Integer preparationTime;
    private Integer servings;

    public RecipeDTO(Integer id, String name, String description, Float calories,
                     List<String> productNames, Integer preparationTime, Integer servings) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.calories = calories;
        this.productNames = productNames;
        this.preparationTime = preparationTime;
        this.servings = servings;
    }

    public RecipeDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getCalories() {
        return calories;
    }

    public void setCalories(Float calories) {
        this.calories = calories;
    }

    public List<String> getProductNames() {
        return productNames;
    }

    public void setProductNames(List<String> productNames) {
        this.productNames = productNames;
    }

    public Integer getPreparationTime() {
        return preparationTime;
    }

    public void setPreparationTime(Integer preparationTime) {
        this.preparationTime = preparationTime;
    }

    public Integer getServings() {
        return servings;
    }

    public void setServings(Integer servings) {
        this.servings = servings;
    }
}
