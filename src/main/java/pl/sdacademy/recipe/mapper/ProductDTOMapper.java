package pl.sdacademy.recipe.mapper;

import org.springframework.stereotype.Component;
import pl.sdacademy.recipe.dto.ProductDTO;
import pl.sdacademy.recipe.model.Product;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class ProductDTOMapper {

    public Product toEntity(ProductDTO productDTO) {
        return Stream.of(productDTO)
                .map(productDTO1 -> {
                    Product product = new Product();
                    product.setName(productDTO.getName());
                    product.setPrice(productDTO.getPrice());
                    product.setType(productDTO.getType());
                    return product;
                }).findAny().orElseThrow(IllegalArgumentException::new);
    }

    public ProductDTO toDTO(Product product) {
        return Stream.of(product)
                .map(product1 -> {
                    return new ProductDTO(product.getId(), product.getName(), product.getType(), product.getPrice());
                }).findAny().orElseThrow(IllegalArgumentException::new);
    }

    public List<ProductDTO> toDTOList(List<Product> products) {
        return products.stream().map(product -> {
            return toDTO(product);
        }).collect(Collectors.toList());
    }

    public List<Product> toEntityList(List<ProductDTO> productDTOS) {
        return productDTOS.stream().map(productDTO -> {
            return toEntity(productDTO);
        }).collect(Collectors.toList());
    }
}
