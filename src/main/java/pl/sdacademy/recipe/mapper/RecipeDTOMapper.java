package pl.sdacademy.recipe.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.sdacademy.recipe.dto.RecipeDTO;
import pl.sdacademy.recipe.model.Product;
import pl.sdacademy.recipe.model.Recipe;
import pl.sdacademy.recipe.repository.ProductRepository;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class RecipeDTOMapper {

    @Autowired
    ProductRepository productRepository;

    public Recipe toEntity(RecipeDTO recipeDTO) {
        List<String> productNames = recipeDTO.getProductNames();
        List<Product> products = productNames.stream()
                .map(productName -> {
                    return productRepository.getByName(productName).orElse(null);
                }).filter(product -> {
                    return product != null;
                }).collect(Collectors.toList());
        return Stream.of(recipeDTO)
                .map(recipeDTO1 -> {
                    return new Recipe(recipeDTO1.getName(), recipeDTO1.getDescription(), products);
                }).findAny().orElseThrow(IllegalArgumentException::new);
    }

    public RecipeDTO toDTO(Recipe recipe) {
        List<Product> products = recipe.getProducts();
        List<String> productNames = products.stream()
                .map(product -> {
                    return product.getName();
                }).collect(Collectors.toList());
        return Stream.of(recipe)
                .map(recipe1 -> {
                    return new RecipeDTO(recipe1.getId(), recipe1.getName(), recipe1.getDescription(), recipe1.getCalories(),
                             productNames, recipe1.getPreparationTime(), recipe1.getServings());
                }).findAny().orElseThrow(IllegalArgumentException::new);
    }

    public List<RecipeDTO> toDTOList(List<Recipe> recipes) {
        return recipes.stream().map(recipe -> {
            return toDTO(recipe);
        }).collect(Collectors.toList());
    }

    public List<Recipe> toEntityList(List<RecipeDTO> recipeDTOS) {
        return recipeDTOS.stream().map(recipeDTO -> {
            return toEntity(recipeDTO);
        }).collect(Collectors.toList());
    }
}