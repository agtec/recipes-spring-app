package pl.sdacademy.recipe.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.sdacademy.recipe.error.NoProductException;
import pl.sdacademy.recipe.mapper.ProductDTOMapper;
import pl.sdacademy.recipe.dto.ProductDTO;
import pl.sdacademy.recipe.model.Product;
import pl.sdacademy.recipe.repository.ProductRepository;

import java.util.List;

@Service
public class ProductService {

    private final ProductRepository productRepository;
    private final ProductDTOMapper productDTOMapper;

    @Autowired
    public ProductService(@Qualifier("productRepository") ProductRepository productRepository,
                          ProductDTOMapper productDTOMapper) {
        this.productRepository = productRepository;
        this.productDTOMapper = productDTOMapper;
    }

    public ProductDTO addProduct(ProductDTO productDTO) {
        Product product = productDTOMapper.toEntity(productDTO);
        return productDTOMapper.toDTO(productRepository.save(product));
    }

    public List<ProductDTO> getProducts() {
        return productDTOMapper.toDTOList(productRepository.findAll());
    }

    public ProductDTO getProduct(Integer id) {
        return productDTOMapper.toDTO(productRepository.findById(id).get());
    }

    public ProductDTO deleteProduct(Integer id) {
        Product product = productRepository.findById(id).orElseThrow(() -> new NoProductException(id.toString()));
        productRepository.deleteById(id);
        return productDTOMapper.toDTO(product);
    }

    public ProductDTO updateProduct(Integer id, ProductDTO productDTO) {
        Product foundProduct = productRepository.findById(id).orElseThrow(() -> new NoProductException(id.toString()));
        foundProduct.setName(productDTO.getName());
        foundProduct.setType(productDTO.getType());
        foundProduct.setPrice(productDTO.getPrice());
        return productDTOMapper.toDTO(foundProduct);
    }

}
