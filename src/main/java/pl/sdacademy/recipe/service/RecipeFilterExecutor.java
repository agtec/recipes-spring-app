package pl.sdacademy.recipe.service;

import pl.sdacademy.recipe.model.Recipe;
import pl.sdacademy.recipe.repository.RecipeRepository;

import java.util.List;

public interface RecipeFilterExecutor {
    List<Recipe> findBy(String value, RecipeRepository recipeRepository);
}
