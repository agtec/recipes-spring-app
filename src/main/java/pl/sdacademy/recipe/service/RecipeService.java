package pl.sdacademy.recipe.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.sdacademy.recipe.error.NoRecipeException;
import pl.sdacademy.recipe.mapper.RecipeDTOMapper;
import pl.sdacademy.recipe.dto.RecipeDTO;
import pl.sdacademy.recipe.model.Product;
import pl.sdacademy.recipe.model.Recipe;
import pl.sdacademy.recipe.repository.ProductRepository;
import pl.sdacademy.recipe.repository.RecipeRepository;

import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
public class RecipeService {

    private final RecipeRepository recipeRepository;

    private final RecipeDTOMapper recipeDTOMapper;

    private final ProductRepository productRepository;

    @Autowired
    public RecipeService(@Qualifier("recipeRepository") RecipeRepository recipeRepository, RecipeDTOMapper recipeDTOMapper,
                         ProductRepository productRepository) {
        this.recipeRepository = recipeRepository;
        this.recipeDTOMapper = recipeDTOMapper;
        this.productRepository = productRepository;
    }

    public RecipeDTO addRecipe(RecipeDTO recipeDTO) {
        Recipe recipe = recipeDTOMapper.toEntity(recipeDTO);
        recipeRepository.save(recipe);
        return recipeDTOMapper.toDTO(recipe);
    }


    public List<RecipeDTO> getAllRecipes() {
        return recipeDTOMapper.toDTOList(recipeRepository.findAll());
    }

    public List<RecipeDTO> findByName(String name) {
        return recipeDTOMapper.toDTOList(recipeRepository.findByName(name));
    }

    public List<RecipeDTO> findByProductName(String productName) {
        return recipeDTOMapper.toDTOList(recipeRepository.findByProducts_name(productName));
    }

    public List<RecipeDTO> findBy(String key, String value) {
        return recipeDTOMapper.toDTOList(RecipeFilter.findBy(key, value, recipeRepository));
    }

    public List<RecipeDTO> findBy(Map<String, String> params) {
        return recipeDTOMapper.toDTOList(RecipeFilter.findBy(params, recipeRepository));
    }

    public List<RecipeDTO> findByPreparationTime(Integer preparationTime) {
        return recipeDTOMapper.toDTOList(recipeRepository.findByPreparationTime(preparationTime));
    }

    public RecipeDTO deleteRecipe(Integer id) {
        Recipe recipe = recipeRepository.findById(id).orElseThrow(() -> new NoRecipeException(id.toString()));
        recipeRepository.deleteById(id);
        return recipeDTOMapper.toDTO(recipe);
    }

    public RecipeDTO updateRecipe(Integer id, RecipeDTO recipeDTO) {
        Recipe foundRecipe = recipeRepository.findById(id).orElseThrow(() -> new NoRecipeException(id.toString()));
        foundRecipe.setName(recipeDTO.getName());
        foundRecipe.setDescription(recipeDTO.getDescription());
        foundRecipe.setServings(recipeDTO.getServings());
        foundRecipe.setPreparationTime(recipeDTO.getPreparationTime());
        List<Product> products = recipeDTO.getProductNames()
                .stream()
                .map(productName -> {
                    return productRepository.getByName(productName).orElseThrow(NoSuchElementException::new);
                }).collect(Collectors.toList());
        foundRecipe.setProducts(products);
        return recipeDTOMapper.toDTO(foundRecipe);
    }

    public RecipeDTO getByName(String name) {
        List<Recipe> recipes = recipeRepository.findByName(name);
        if (recipes.isEmpty()) {
            throw new NoRecipeException(name);
        }
        return recipeDTOMapper.toDTO(recipes.get(0));
    }

}