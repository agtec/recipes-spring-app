package pl.sdacademy.recipe.service;

import pl.sdacademy.recipe.model.Recipe;
import pl.sdacademy.recipe.repository.RecipeRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public enum RecipeFilter {
    BY_DESCRIPTION("description", (value, repository) -> {
        return repository.findByDescription(value);
    }),
    BY_CALORIES("calories", (value, executor) -> {
        float calories = Float.parseFloat(value);
        return executor.findByCalories(calories);
    }),
    BY_PREPARATION_TIME("preparationTime", (value, repository) -> {
        int preparationTime = Integer.parseInt(value);
        return repository.findByPreparationTime(preparationTime);
    }),
    BY_SERVINGS("servings", (value, repository) -> {
        int servings = Integer.parseInt(value);
        return repository.findByServings(servings);
    }),
    BY_PRODUCT_NAMES("products", (value, repository) -> {
        return repository.findByProducts_name(value);
    }),
    UNKNOWN("", (value, repository) -> {
        return Collections.emptyList();
    });

    private RecipeFilterExecutor filterExecutor;
    private String key;

    RecipeFilter(String key, RecipeFilterExecutor filterExecutor) {
        this.key = key;
        this.filterExecutor = filterExecutor;
    }

    public static List<Recipe> findBy(String key, String value, RecipeRepository recipeRepository) {
        return Stream.of(values())
                .filter(obj -> obj.key.equals(key))
                .findAny()
                .orElse(UNKNOWN).filterExecutor.findBy(value, recipeRepository);
    }

    public static List<Recipe> findBy(Map<String, String> map, RecipeRepository recipeRepository) {
        List<Recipe> recipes = new ArrayList<>();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            List<Recipe> recipeList = findBy(key, value, recipeRepository);
            recipes.addAll(recipeList);
        }
        return recipes;
    }
}