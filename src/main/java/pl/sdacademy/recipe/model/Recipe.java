package pl.sdacademy.recipe.model;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.List;

@Entity
public class Recipe {

    @Id
    @GeneratedValue
    private Integer id;

    @NotNull
    @Size(min = 2)
    @Size(max = 40)
    private String name;

    @NotNull
    @NotBlank
    @Size(min = 10)
    @Size(max = 100)
    private String description;

    //@Max(value = 20000)
    private Float calories;

    @NotNull
    @ElementCollection
    @OneToMany
    private List<Product> products;

    @Min(15)
    @Max(240)
    private Integer preparationTime;

    @Min(1)
    @Max(10)
    private Integer servings;

    public Recipe() {
    }

    public Recipe(Integer id, String name, String description, Float calories, List<Product> products,
                  Integer preparationTime, Integer servings) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.calories = calories;
        this.products = products;
        this.preparationTime = preparationTime;
        this.servings = servings;
    }

    public Recipe(@NotNull @Size(min = 2) @Size(max = 40) String name, @NotNull @NotBlank @Size(min = 10) @Size(max = 100) String description,
                  List<Product> products) {
        this.name = name;
        this.description = description;
        this.products = products;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getCalories() {
        return calories;
    }

    public void setCalories(Float calories) {
        this.calories = calories;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public Integer getPreparationTime() {
        return preparationTime;
    }

    public void setPreparationTime(Integer preparationTime) {
        this.preparationTime = preparationTime;
    }

    public Integer getServings() {
        return servings;
    }

    public void setServings(Integer servings) {
        this.servings = servings;
    }

    @Override
    public String toString() {
        return "Recipe{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", calories=" + calories +
                ", ingredients=" + products +
                ", preparationTime=" + preparationTime +
                ", Servings=" + servings +
                '}';
    }
}
