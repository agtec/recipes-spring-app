package pl.sdacademy.recipe.error;

public class NoRecipeException extends RuntimeException{

    public NoRecipeException(String name) {
        super(String.format("No recipe with %s name found!", name));
    }
}
