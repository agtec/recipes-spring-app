package pl.sdacademy.recipe.error;

public class NoProductException extends RuntimeException {

    public NoProductException(String name) {
        super(String.format("No product with %s name found!", name));
    }
}
