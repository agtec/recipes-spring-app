package pl.sdacademy.recipe.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpStatusCodeException;
import pl.sdacademy.recipe.dto.ProductDTO;
import pl.sdacademy.recipe.service.ProductService;

import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @PostMapping
    public ProductDTO addProduct(@RequestBody ProductDTO productDTO) {
        return productService.addProduct(productDTO);
    }

    @GetMapping
    public List<ProductDTO> getProducts() {
        return productService.getProducts();
    }

    @GetMapping("/{id}")
    public ProductDTO getProduct(@PathVariable("id") Integer id) {
        return productService.getProduct(id);
    }

    @DeleteMapping(value = "/{id}")
    public ProductDTO deleteProduct(@PathVariable("id") Integer id) throws HttpStatusCodeException {
        return productService.deleteProduct(id);
    }

    @PutMapping("/{id}")
    public ProductDTO update(@PathVariable("id") Integer id, @RequestBody ProductDTO productDTO) {
        return productService.updateProduct(id, productDTO);
    }
}
