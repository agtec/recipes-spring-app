package pl.sdacademy.recipe.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpStatusCodeException;
import pl.sdacademy.recipe.dto.RecipeDTO;
import pl.sdacademy.recipe.service.RecipeService;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/recipes")
public class RecipeController {

    @Autowired
    private RecipeService recipeService;

    @PostMapping
    public RecipeDTO addRecipe(@Valid @RequestBody RecipeDTO recipeDTO) {
        return recipeService.addRecipe(recipeDTO);
    }

    @GetMapping(value = "/getByRecipeName")
    public List<RecipeDTO> findByRecipeName(@RequestParam String recipeName) {
        return recipeService.findByName(recipeName);
    }

    @GetMapping(value = "/getByProductName")
    public List<RecipeDTO> findByProductName(@RequestParam String productName) {
        return recipeService.findByProductName(productName);
    }

    @GetMapping("/getByParameter")
    public List<RecipeDTO> getRecipesByParameter(@RequestParam(required = false) String key, String value) {
        return recipeService.findBy(key, value);
    }

    @GetMapping("/getByParameters")
    public List<RecipeDTO> getRecipesByParameters(@RequestParam(required = false) Map<String, String> params) {
        return recipeService.findBy(params);
    }

    @GetMapping(value = "/getByPreparationTime")
    public List<RecipeDTO> findByPreparationTime(@RequestParam Integer preparationTime) {
        return recipeService.findByPreparationTime(preparationTime);
    }

    @DeleteMapping(value = "/{id}")
    public RecipeDTO deleteRecipe(@PathVariable("id") Integer id) throws HttpStatusCodeException {
        return recipeService.deleteRecipe(id);
    }

    @GetMapping("/{name}")
    public RecipeDTO getByName(@PathVariable("name") String name) {
        return recipeService.getByName(name);
    }

    @PutMapping("/{id}")
    public RecipeDTO update(@PathVariable("id") Integer id, @RequestBody RecipeDTO recipe) {
        return recipeService.updateRecipe(id, recipe);
    }

    @GetMapping
    public List<RecipeDTO> getRecipes() {
        return recipeService.getAllRecipes();
    }

}
